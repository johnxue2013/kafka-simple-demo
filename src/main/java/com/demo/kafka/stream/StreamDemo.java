package com.demo.kafka.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author han.xue
 * @since 2018-02-25 20:38:38
 */
public class StreamDemo {
	public static void main(String[] args) {
		Map<String, Object> props = new HashMap<>();

		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "my-stream-processing-application");
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		props.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

		StreamsConfig config = new StreamsConfig(props);

		KStreamBuilder builder = new KStreamBuilder();
	}
}
