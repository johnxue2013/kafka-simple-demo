package com.demo.kafka.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @author han.xue
 * @since 2017-12-25 14:18:18
 */
public class IPUtil {

	private static final String DEFAULT_IP = "127.0.0.1";

	/**
	 * 获取本机IP
	 * @return
	 */
	public static String getLocalHostIP(){

		Enumeration allNetInterfaces;
		try {
			allNetInterfaces = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			e.printStackTrace();
			return DEFAULT_IP;
		}
		InetAddress ip;
		while (allNetInterfaces.hasMoreElements())
		{
			NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
			Enumeration addresses = netInterface.getInetAddresses();
			while (addresses.hasMoreElements())
			{
				ip = (InetAddress) addresses.nextElement();
				if (ip instanceof Inet4Address && !ip.getHostAddress().startsWith("127"))
				{
					return ip.getHostAddress();
				}
			}
		}
		return DEFAULT_IP;
	}
}
