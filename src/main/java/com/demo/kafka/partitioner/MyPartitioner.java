package com.demo.kafka.partitioner;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 * 自定义分区算法
 * @author han.xue
 * @since 2018-05-15 09:32:32
 */
public class MyPartitioner implements Partitioner {
	@Override
	public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {

		if (null != key) {
			int numPartitions = cluster.partitionsForTopic(topic).size();
			int partition;
			try {
				partition = Integer.parseInt(key.toString()) % numPartitions;
			} catch (Exception e) {
				partition = key.toString().hashCode() % numPartitions;
			}

			return partition;
		}

		return 0;
	}

	@Override
	public void close() {

	}

	@Override
	public void configure(Map<String, ?> configs) {

	}
}
