package com.demo.kafka.product;

import com.demo.kafka.util.IPUtil;
import org.apache.kafka.clients.producer.*;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 发送消息到指定的分区
 *
 * @author han.xue
 * @since 2018-05-15 11:18:18
 */
public class Producer2SpecialPartition {

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IPUtil.getLocalHostIP() + ":9092");

		//开启生产者幂等属性，设置此值需要同时要求其他属性值设置为特定的值，
		// 具体要求可以点击ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG进入查看源码
		//幂等性保证了消息只会被broker接受一次
		//props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true);

		//ack是判别请求是否完整的条件（就是判断是否发送成功了）。此处的all将会阻塞消息，
		//这种性能最低，但是最可靠
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		//如果请求失败，生产者会自动重试，我们指定是0次，如果启用重试，
		//则会有重复消息的可能性。
		props.put(ProducerConfig.RETRIES_CONFIG, 0);
		//(生产者)缓存每个分区未发送消息。缓存的大小是通过 batch.size
		//配置指定的。值较大的话将会产生更大的批。并需要更多的内存（因为每个“活跃”的分区都有1个缓冲区）。
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
		//当缓冲区用尽，额外的发送将被阻塞，阻塞时间的临界值由此值控制，之后将抛出TimeoutException
		props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
		props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 60 * 5);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, "com.demo.kafka.partitioner.MyPartitioner");

		Producer<String, String> producer = new KafkaProducer<>(props);
		for (int i = 0; i < 10; i++) {
			//send方法是异步的,添加消息到缓冲区等待发送，并立即返回,后者将单个消息批量在一起发送
			//来提高效率
			Future<RecordMetadata> send = producer.send(new ProducerRecord<>("my-topic", Integer.toString(i), Integer
					.toString(i)), new Callback() {
				//callback运行在生产者的I/O线程中，所以是相当快的，但是如果需要执行阻塞或者昂贵的回调，建议
				//使用在callback中使用自己的Executor来并行处理
				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					if (exception != null) {
						exception.printStackTrace();
					}

					System.out.printf("The partition of record we just send is:  %d , The offset of the record we just sent is: %s%n ",metadata.partition(), metadata.offset());
				}
			});

			//将阻塞，直至元数据返回
			//完全无阻赛应使用上述的callback
//			RecordMetadata recordMetadata = send.get();
		}

		System.out.println("---------------------------------------------------------------------");
		producer.close();

		/*
		 生产者需要leader确认请求完成之前接收的应答数。此配置控制了发送消息的耐用性，支持以下配置：
		 acks=0 如果设置为0，那么生产者将不等待任何消息确认。消息将立刻天际到socket缓冲区并考虑发送。在这种情况下不能保障消息被服务器接收到。并且重试机制不会生效（因为客户端不知道故障了没有）。每个消息返回的offset始终设置为-1。
		 acks=1，这意味着leader写入消息到本地日志就立即响应，而不等待所有follower应答。在这种情况下，如果响应消息之后但follower还未复制之前leader立即故障，那么消息将会丢失。
		 acks=all 这意味着leader将等待所有副本同步后应答消息。此配置保障消息不会丢失（只要至少有一个同步的副本或者）。这是最强壮的可用性保障。等价于acks=-1。
		 */
	}
}
