package com.demo.kafka.product;

import com.demo.kafka.util.IPUtil;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.AuthorizationException;
import org.apache.kafka.common.errors.OutOfOrderSequenceException;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * 事务性producer demo
 *
 * @author han.xue
 * @since 2018-06-05 18:06:06
 */
public class ProducerWithTransaction {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IPUtil.getLocalHostIP() + ":9092");
		props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "my-transactional-id");
		Producer<String, String> producer = new KafkaProducer<>(props, new StringSerializer(), new StringSerializer());

		producer.initTransactions();

		try {
			producer.beginTransaction();
			for (int i = 0; i < 100; i++) {
				producer.send(new ProducerRecord<>("my-topic-transactional", Integer.toString(i), Integer.toString(i)), new Callback() {
					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception != null) {
							exception.printStackTrace();
						}

						System.out.printf("The partition of record we just send is:  %d , The offset of the record we just sent is: %s%n ",metadata.partition(), metadata.offset());
					}
				});

//				if(i == 99) {
//					throw new KafkaException("手动抛出异常异常");
//				}
			}

			producer.commitTransaction();
		} catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
			// We can't recover from these exceptions, so our only option is to close the producer and exit.
			e.printStackTrace();
			producer.close();
		} catch (KafkaException e) {
			// For all other exceptions, just abort the transaction and try again.
			e.printStackTrace();
			producer.abortTransaction();
		}
		producer.close();
	}
}
