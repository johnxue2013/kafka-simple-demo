package com.demo.kafka.consumer;

import com.demo.kafka.util.IPUtil;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * 手动提交offset的示例
 *
 * @author han.xue
 * @since 2017-12-24 20:24:24
 */
public class ConsumerWithManualCommit {
	private static final int BATCH_SIZE = 100;

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, IPUtil.getLocalHostIP() + ":9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "10000");
		props.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, "3000");
		props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "30000");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Collections.singletonList("my-topic"));

		List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
				buffer.add(record);
			}
			if (buffer.size() >= BATCH_SIZE) {
				//做一写实际业务逻辑
				insertIntoDb(buffer);
				//手动提交偏移量(同步提交)
				consumer.commitSync();
				buffer.clear();
			}
		}
	}


	private static void insertIntoDb(List<ConsumerRecord<String, String>> buffer) {
		System.out.println(buffer);
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}


}
