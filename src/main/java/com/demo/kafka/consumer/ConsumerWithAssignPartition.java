package com.demo.kafka.consumer;

import com.demo.kafka.util.IPUtil;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.util.Arrays;
import java.util.Properties;

/**
 * 手动指定消费者只消费topic的指定分区
 *
 * @author han.xue
 * @since 2018-06-06 11:13:13
 */
public class ConsumerWithAssignPartition {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, IPUtil.getLocalHostIP() + ":9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

		String topic = "my-topic";
		TopicPartition topicPartition0 = new TopicPartition(topic, 0);
		TopicPartition topicPartition1 = new TopicPartition(topic, 1);

		consumer.assign(Arrays.asList(topicPartition0, topicPartition1));
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
				System.out.printf("partition = %d, Consumer offset = %d, key = %s, value = %s%n", record.partition(),
						record.offset(), record.key(), record.value());
			}
		}

		/*
		一旦手动分配分区，你可以在循环中调用poll（跟前面的例子一样）。消费者分组仍需要提交offset，
		只是现在分区的设置只能通过调用assign修改，因为手动分配不会进行分组协调，因此消费者故障不会引发分区重新平衡。
		每一个消费者是独立工作的（即使和其他的消费者共享GroupId）。为了避免offset提交冲突，
		通常你需要确认每一个consumer实例的groupId都是唯一的。

		注意，手动分配分区（即，assign）和动态分区分配的订阅topic模式（即，subscribe）不能混合使用
		*/
	}
}
